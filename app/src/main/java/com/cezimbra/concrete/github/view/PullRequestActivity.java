package com.cezimbra.concrete.github.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.cezimbra.concrete.github.R;
import com.cezimbra.concrete.github.adapter.RepositorioAdapter;
import com.cezimbra.concrete.github.adapter.RequestAdapter;
import com.cezimbra.concrete.github.controller.RepositorioBO;
import com.cezimbra.concrete.github.interfac.WebServiceCallback;
import com.cezimbra.concrete.github.listener.EndlessRepositorioListener;
import com.cezimbra.concrete.github.model.Repositorio;
import com.cezimbra.concrete.github.model.Requests;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.InjectView;

public class PullRequestActivity extends BaseActivity implements WebServiceCallback{

    private static String login;
    private static String name;

    @InjectView(R.id.rvList)
    RecyclerView rvList;
    RequestAdapter adapter;
    ArrayList<Requests> requests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this) {
            @Override
            protected int getExtraLayoutSpace(RecyclerView.State state) {
                return 300;
            }
        };
        rvList.setLayoutManager(linearLayoutManager);

        requests = new ArrayList<>();
        adapter = new RequestAdapter(this, requests);
        rvList.setAdapter(adapter);

        adapter.updateItems(true);
        RepositorioBO.getPullRequests(PullRequestActivity.this, PullRequestActivity.this, login, name, 1);
    }

    public static void startActivity(Context context, String _login, String _name) {
        login = _login;
        name = _name;
        context.startActivity(new Intent(context, PullRequestActivity.class));
    }

    @Override
    public void onResult(String result) {
        Gson gson = new Gson();
        try {

            JSONArray arr = new JSONArray(result);

            for (int i = 0; i < arr.length(); i++) {
                Requests rep = gson.fromJson(arr.getJSONObject(i).toString(), Requests.class);
                requests.add(rep);
            }
            adapter.notifyDataSetChanged();
            //listener.isLoading(false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onServiceUnavaliableOrNotFound(String error) {

    }

    @Override
    public void onNoInternet(String error) {

    }

    @Override
    public void onProgress(int size, int total) {

    }
}

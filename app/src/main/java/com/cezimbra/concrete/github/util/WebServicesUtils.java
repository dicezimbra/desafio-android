package com.cezimbra.concrete.github.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.cezimbra.concrete.github.interfac.WebServiceCallback;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Vector;


/**
 * Created by lucasferreiramachado on 05/09/15.
 */
public class WebServicesUtils {
    public static final String IP = "api.github.com";

    protected static final String VERSION_1 = "/v1";
    protected static final String VERSION_2 = "/v2";
    protected static final String VERSION_3 = "/v3";
    protected static final String VERSION_4 = "/v4";
    protected static final String VERSION_5 = "/v5";
    protected static final String VERSION_6 = "/v6";
    protected static final String VERSION_8 = "/v8";
    protected static final String PROTOCOL = "https://";

    public static boolean isOnline(Context act) {
        if (act == null) {
            return true;
        }
        ConnectivityManager cm = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public static String getBaseUrl() {

        StringBuffer url = new StringBuffer();
        url.append(PROTOCOL);
        url.append(IP);


        return url.toString();
    }

    public static String getBaseUrlV1() {

        StringBuffer url = new StringBuffer();
        url.append(PROTOCOL);
        url.append(IP);
        url.append(VERSION_1);

        return url.toString();
    }

    public static String getBaseUrlV2() {

        StringBuffer url = new StringBuffer();
        url.append(PROTOCOL);
        url.append(IP);
        url.append(VERSION_2);

        return url.toString();
    }

    public static String getBaseUrlV3() {

        StringBuffer url = new StringBuffer();
        url.append(PROTOCOL);
        url.append(IP);
        url.append(VERSION_3);

        return url.toString();
    }

    public static String getBaseUrlV4() {

        StringBuffer url = new StringBuffer();
        url.append(PROTOCOL);
        url.append(IP);
        url.append(VERSION_4);

        return url.toString();
    }

    public static String getBaseUrlV5() {

        StringBuffer url = new StringBuffer();
        url.append(PROTOCOL);
        url.append(IP);
        url.append(VERSION_5);

        return url.toString();
    }

    public static String getBaseUrlV6() {

        StringBuffer url = new StringBuffer();
        url.append(PROTOCOL);
        url.append(IP);
        url.append(VERSION_6);

        return url.toString();
    }
    public static String getBaseUrlV8() {

        StringBuffer url = new StringBuffer();
        url.append(PROTOCOL);
        url.append(IP);
        url.append(VERSION_8);

        return url.toString();
    }

    public static String getUrlQuery(String mBaseUrl, Vector<String> params) {


        String url = mBaseUrl;

        int size = params.size();
        if (size == 0) {
            return url;
        }

        url = url.concat("?");

        for (int i = 0; i < size; i++) {
            try {
                url = url.concat("p" + (i + 1));
                url = url.concat("=");
                String value = params.get(i);
                if (value != null) {
                    value = encodeParam(value);
                }
                if (value == null) {
                    value = "";
                }
                url = url.concat(value);
                if (i + 1 < size) {
                    url = url.concat("&");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        //Log.e("MHttpRequisition", "url = " + url);
        return url;
    }

    private static String encodeParam(String param) {
        try {
            if (param == null) {

                return null;
            }
            return URLEncoder.encode(param, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return param;
        }
    }

    public static int get(final Context ctx, final String url, final WebServiceCallback c) {

        if (!isOnline(ctx)) {
            if (c != null) {
                c.onNoInternet("No internet");
            }
            return 1000;
        } else {

            AsyncHttpClient client = new AsyncHttpClient();
            client.setMaxRetriesAndTimeout(3, 4000);
            client.setUserAgent("User-Agent: dicezimbra");
            client.setMaxConnections(1);
            client.setConnectTimeout(30000);
            client.setResponseTimeout(60000);

            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    // Initiated the request
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.e("onSuccess", "statusCode =" + statusCode + " responseString = " + responseString);
                    try {
                        if (c != null)
                            c.onResult(responseString);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.e("onFailure", "statusCode =" + statusCode + " throwable = " + throwable);
                    if (throwable != null)
                        throwable.printStackTrace();

                    String error = "";
                    if (statusCode == 503 || statusCode == 404) {

                        if (c != null) {
                            c.onServiceUnavaliableOrNotFound("Serviço indisponível");
                        }
                        return;
                    }

                    error = "Comunnication error. ";

                    if (statusCode > 400) {
                        error = "HTTP " + statusCode + ". ";
                    }

                    if (throwable != null) {
                        if (throwable.getLocalizedMessage() != null) {
                            error = error + "" + throwable.getLocalizedMessage();
                        } else if (throwable.getMessage() != null) {
                            error = error + "" + throwable.getMessage();
                        }
                    }


                    try {
                        if (c != null) {
                            c.onError(error);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onRetry(int retryNo) {
                    super.onRetry(retryNo);
                }

                @Override
                public void onFinish() {
                    // Completed the request (either success or failure)
                }
            });

        }
        return 0;
    }

    public static AsyncHttpClient syncHttpClient = new SyncHttpClient();
    public static AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

    private static AsyncHttpClient getClient()

    {
        // Return the synchronous HTTP client when the thread is not prepared
        if (Looper.myLooper() == null)
            return syncHttpClient;
        return asyncHttpClient;
    }

    public static int post(final Activity act, StringEntity entity, final String url, final WebServiceCallback c) {

        if (!isOnline(act)) {
            if (c != null) {

                c.onNoInternet("No internet");
            }
            return 1000;
        } else {
            //Log.e("postInputStreamFromUrl", "c =" + c + " url =" + url);


            getClient().addHeader("Content-length", "" + entity.getContentLength());
            getClient().setMaxRetriesAndTimeout(5, 1000);

            getClient().setMaxConnections(1);
            getClient().setConnectTimeout(120000);
            getClient().setResponseTimeout(120000);

            getClient().post(act, url, entity, "application/json; charset=utf-8", new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    act.finish();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    try {

                        if (c != null) {
                            c.onResult(responseString);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);

                    if (c != null) {
                        c.onProgress((int) bytesWritten, (int) totalSize);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    //Log.e("postInputStreamFromUrl", "onFailure throwable = " + throwable);
                    //statusCode = 503;

                    if (throwable != null)
                        throwable.printStackTrace();

                    String error = "";
                    if (statusCode == 503 || statusCode == 404) {
                        error = "404";
                        Toast.makeText(act, error, Toast.LENGTH_LONG).show();
                        if (c != null) {
                            c.onServiceUnavaliableOrNotFound("404");
                        }

                        return;
                    }

                    error = "Comunnication error. ";

                    if (statusCode > 400) {
                        error = "HTTP " + statusCode + ". ";
                    }

                    /*
                    if(responseString!=null && responseString.length()<=40){
                        error = error+""+responseString+". ";
                    }*/

                    if (throwable != null) {
                        if (throwable.getLocalizedMessage() != null) {
                            error = error + "" + throwable.getLocalizedMessage();
                        } else if (throwable.getMessage() != null) {
                            error = error + "" + throwable.getMessage();
                        }
                    }


                    try {
                        if (c != null) {
                            c.onError(error);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onRetry(int retryNo) {
                    super.onRetry(retryNo);
                }

                @Override
                public void onFinish() {
                    // Completed the request (either success or failure)
                }
            });
        }
        return 0;
    }

    public static StringEntity postEntity(Vector<String> params) {


        JSONObject jobj = new JSONObject();
        try {
            for (int i = 0; i < params.size(); i++) {
                if (params.get(i) != null) {
                    jobj.put("p" + (i + 1), params.get(i));
                }
            }


            StringEntity entity = new StringEntity(new String(jobj.toString().getBytes("UTF-8"), "UTF-8"), HTTP.UTF_8);
            entity.setContentType("application/json; charset=utf-8");
            return entity;

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static StringEntity postEntity(JSONObject paramjobj) {
        JSONObject jobj = paramjobj;
        try {

            StringEntity entity = new StringEntity(new String(jobj.toString().getBytes("UTF-8"), "UTF-8"), HTTP.UTF_8);
            entity.setContentType("application/json; charset=utf-8");
            return entity;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}

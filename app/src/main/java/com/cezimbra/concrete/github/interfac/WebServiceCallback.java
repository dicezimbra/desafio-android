package com.cezimbra.concrete.github.interfac;

/**
 * Created by diego on 24/02/2015.
 */
public interface WebServiceCallback {
    public void onResult(String result);
    public void onError(String error);
    public void onServiceUnavaliableOrNotFound(String error);
    public void onNoInternet(String error);
    public void onProgress(int size, int total);
}

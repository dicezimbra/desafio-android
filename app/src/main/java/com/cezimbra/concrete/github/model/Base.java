
package com.cezimbra.concrete.github.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Base {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("ref")
    @Expose
    private String ref;
    @SerializedName("sha")
    @Expose
    private String sha;
    @SerializedName("user")
    @Expose
    private User__ user;
    @SerializedName("repo")
    @Expose
    private Repo_ repo;

    /**
     * 
     * @return
     *     The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 
     * @param label
     *     The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 
     * @return
     *     The ref
     */
    public String getRef() {
        return ref;
    }

    /**
     * 
     * @param ref
     *     The ref
     */
    public void setRef(String ref) {
        this.ref = ref;
    }

    /**
     * 
     * @return
     *     The sha
     */
    public String getSha() {
        return sha;
    }

    /**
     * 
     * @param sha
     *     The sha
     */
    public void setSha(String sha) {
        this.sha = sha;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User__ getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User__ user) {
        this.user = user;
    }

    /**
     * 
     * @return
     *     The repo
     */
    public Repo_ getRepo() {
        return repo;
    }

    /**
     * 
     * @param repo
     *     The repo
     */
    public void setRepo(Repo_ repo) {
        this.repo = repo;
    }



}

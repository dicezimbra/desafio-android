package com.cezimbra.concrete.github.view;

import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.cezimbra.concrete.github.R;
import com.cezimbra.concrete.github.adapter.RepositorioAdapter;
import com.cezimbra.concrete.github.controller.RepositorioBO;
import com.cezimbra.concrete.github.interfac.WebServiceCallback;
import com.cezimbra.concrete.github.listener.EndlessRepositorioListener;
import com.cezimbra.concrete.github.model.Repositorio;
import com.cezimbra.concrete.github.util.TempUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.InjectView;

public class MainActivity extends BaseActivity implements WebServiceCallback {

    @InjectView(R.id.rvList)
    RecyclerView rvList;

    private RepositorioAdapter adapter;
    private ArrayList<Repositorio> repositorios;
    private int page = 1;
    private EndlessRepositorioListener listener = null;
    private LinearLayoutManager linearLayoutManager;
    private String LIST_STATE_KEY = "layout_manager";
    private String LIST = "array_list";
    private Parcelable mListState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayoutManager = new LinearLayoutManager(this) {
            @Override
            protected int getExtraLayoutSpace(RecyclerView.State state) {
                return 300;
            }
        };
        rvList.setLayoutManager(linearLayoutManager);


        /*if (savedInstanceState != null && (repositorios == null || repositorios.size() == 0)) {
            repositorios = TempUtil.getRepositories(this);
        }*/

        if (savedInstanceState != null) {
            repositorios = (ArrayList<Repositorio>) savedInstanceState.getSerializable(LIST);
        } else {
            repositorios = new ArrayList<>();
        }

        Log.e("Github", "MainActivity on create repositorios = " + repositorios);

        if (repositorios == null)
            repositorios = new ArrayList<>();


        adapter = new RepositorioAdapter(this, repositorios);
        rvList.setAdapter(adapter);

        listener = new EndlessRepositorioListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                RepositorioBO.getRepositories(MainActivity.this, current_page, MainActivity.this);
            }
        };

        rvList.addOnScrollListener(listener);

        adapter.updateItems(true);

        if (repositorios.size() == 0)
            RepositorioBO.getRepositories(this, page, this);
        loadList(TempUtil.getRepositories(this));
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mListState = linearLayoutManager.onSaveInstanceState();
        outState.putParcelable(LIST_STATE_KEY, mListState);
        outState.putSerializable(LIST, repositorios);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mListState = linearLayoutManager.onSaveInstanceState();
        outState.putParcelable(LIST_STATE_KEY, mListState);
        outState.putSerializable(LIST, repositorios);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mListState != null) {
            linearLayoutManager.onRestoreInstanceState(mListState);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //TempUtil.setRepositories(this, repositorios);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mListState = savedInstanceState.getParcelable(LIST_STATE_KEY);

            ArrayList<Repositorio> temp = (ArrayList<Repositorio>) savedInstanceState.getSerializable(LIST);
            if (temp != null)
                repositorios = temp;
        }
    }

    @Override
    public void onResult(String result) {

        String oldResult = TempUtil.getRepositories(this);

        if (oldResult != null && oldResult.equals(result)) {
            return;
        }

        Gson gson = new Gson();

        try {
            ArrayList<Repositorio> temp1 = gson.fromJson(oldResult, new TypeToken<ArrayList<Repositorio>>() {
            }.getType());
            ArrayList<Repositorio> temp2 = gson.fromJson(result, new TypeToken<ArrayList<Repositorio>>() {
            }.getType());

            if (temp1.equals(temp2)) {
                return;
            }

            if (temp1.get(0).equals(temp2.get(0))) {
                return;
            }

            if (temp1.get(0).toString().equals(temp2.get(0).toString())) {
                return;
            }
        }catch (Exception e){

        }

        loadList(result);

    }

    private void loadList(String result) {
        if(result == null){
            return;
        }

        Gson gson = new Gson();
        JSONObject obj = null;
        try {
            obj = new JSONObject(result);

            JSONArray arr = obj.getJSONArray("items");

            for (int i = 0; i < arr.length(); i++) {
                Repositorio rep = gson.fromJson(arr.getJSONObject(i).toString(), Repositorio.class);
                repositorios.add(rep);
            }

            TempUtil.setRepositories(this, result);
            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            showMsg("Erro ao ler os dados");
        }
    }

    @Override
    public void onError(String error) {
        Log.e("Github", "MainActviity onError error = " + error);
        showMsg("MainActviity onError error = " + error);
    }

    @Override
    public void onServiceUnavaliableOrNotFound(String error) {
        Log.e("Github", "MainActviity onServiceUnavaliableOrNotFound error = " + error);
        showMsg("MainActviity onServiceUnavaliableOrNotFound error = " + error);
    }

    @Override
    public void onNoInternet(String error) {
        Log.e("Github", "MainActviity onNoInternet error = " + error);
        showMsg("MainActviity onNoInternet error = " + error);
    }

    @Override
    public void onProgress(int size, int total) {
        Log.e("Github", "MainActviity onProgress size = " + size + " total = " + total);
        showMsg("MainActviity onProgress size = " + size + " total = " + total);
    }
}

package com.cezimbra.concrete.github.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.cezimbra.concrete.github.R;
import com.cezimbra.concrete.github.model.Repositorio;
import com.cezimbra.concrete.github.util.Utils;
import com.cezimbra.concrete.github.view.PullRequestActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by froger_mcs on 05.11.14.
 */

public class RepositorioAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final int VIEW_TYPE_DEFAULT = 1;
    private static final int VIEW_TYPE_LOADER = 2;

    private static final int ANIMATED_ITEMS_COUNT = 2;
    private Context context;
    private int lastAnimatedPosition = -1;
    private boolean aanimateItems = false;
    private boolean showLoadingView = false;
    private int loadingViewSize = Utils.dpToPx(200);
    private ArrayList<Repositorio>  repositorios;

    public RepositorioAdapter(Context context, ArrayList<Repositorio> repositorios) {
        this.context = context;
        this.repositorios = repositorios;
        if (repositorios == null) {
            repositorios = new ArrayList<>();
        }
    }

    public class ListItem {
        public CellRepoViewHolder holder;
        public Repositorio repo;
    }


    private void bindDefaultFeedItem(int position, CellRepoViewHolder holder) {

        ListItem listItem = new ListItem();
        listItem.holder = holder;

        try {
            final Repositorio repo = repositorios.get(position);


            holder.repoName.setText(repo.getFull_name());
            holder.repoDescription.setText(repo.getDescription());
            holder.authorName.setText(repo.getOwner().getLogin());
            holder.forks.setText(repo.getForks_count());
            holder.stars.setText(repo.getStargazers_count());

            final View progress = holder.progress;
            Picasso.with(context)

                    .load(repo.getOwner().getAvatar_url())
                    .into(holder.authorPicture, new Callback() {
                        @Override
                        public void onSuccess() {
                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });

            holder.click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PullRequestActivity.startActivity(context, repo.getOwner().getLogin(), repo.getName());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void bindLoadingFeedItem(final CellRepoViewHolder holder) {

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_repositorio, parent, false);
        final CellRepoViewHolder cellFeedViewHolder = new CellRepoViewHolder(view);

        if (viewType == VIEW_TYPE_DEFAULT) {



        } else if (viewType == VIEW_TYPE_LOADER) {
            View bgView = new View(context);
            bgView.setLayoutParams(new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
            ));
            bgView.setBackgroundColor(0x77ffffff);


            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(loadingViewSize, loadingViewSize);
            params.gravity = Gravity.CENTER;

        }

        return cellFeedViewHolder;
    }

    private void runEnterAnimation(View view, int position) {
        if (!aanimateItems || position >= ANIMATED_ITEMS_COUNT - 1) {
            return;
        }

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(Utils.getScreenHeight(context));
            view.animate()
                    .translationY(0)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(700)
                    .start();
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        runEnterAnimation(viewHolder.itemView, position);
        final CellRepoViewHolder holder = (CellRepoViewHolder) viewHolder;
        if (getItemViewType(position) == VIEW_TYPE_DEFAULT) {
            bindDefaultFeedItem(position, holder);
        } else if (getItemViewType(position) == VIEW_TYPE_LOADER) {
            bindLoadingFeedItem(holder);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (showLoadingView && position == 0) {
            return VIEW_TYPE_LOADER;
        } else {
            return VIEW_TYPE_DEFAULT;
        }
    }

    @Override
    public int getItemCount() {
        if (repositorios != null)
            return repositorios.size();

        return 0;
    }

    public void updateItems(boolean animated) {
        aanimateItems = animated;
        notifyDataSetChanged();
    }

    public void showLoadingView() {
        showLoadingView = true;
        notifyItemChanged(0);
    }

    public static class CellRepoViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.nomeRepositorio)
        TextView repoName;

        @InjectView(R.id.descricaoRepositorio)
        TextView repoDescription;

        @InjectView(R.id.nomeAutor)
        TextView authorName;

        @InjectView(R.id.fotoAutor)
        ImageView authorPicture;

        @InjectView(R.id.estrelas)
        TextView stars;

        @InjectView(R.id.forks)
        TextView forks;

        @InjectView(R.id.click)
        View click;

        @InjectView(R.id.progress)
        fr.castorflex.android.circularprogressbar.CircularProgressBar progress;
        public CellRepoViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

}

package com.cezimbra.concrete.github.util;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.cezimbra.concrete.github.model.Repositorio;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Created by cezimbra on 29/05/2016.
 */
public class TempUtil {

    public static String getRepositories(Activity act) {


        SharedPreferences shared = act.getSharedPreferences("bd", act.MODE_PRIVATE);
        if (shared == null) {
            return null;
        }

        String json = shared.getString("repo", null);
        Log.e("Github", "MainActivity on create repositorios  getRepositories json = " + json);

        return json;

    }

    public static int setRepositories(Activity act, String repositories) {


        SharedPreferences shared = act.getSharedPreferences("bd", act.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString("repo", repositories);
        editor.commit();
        return 0;


    }
}

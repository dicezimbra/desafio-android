package com.cezimbra.concrete.github.controller;


import android.app.Activity;
import android.util.Log;

import com.cezimbra.concrete.github.interfac.WebServiceCallback;
import com.cezimbra.concrete.github.util.WebServicesUtils;
import com.cezimbra.concrete.github.view.PullRequestActivity;

/**
 * Created by cezimbra on 27/05/2016.
 */
public class RepositorioBO {

   /* public static void getRepositories(){
        GithubEndpointInterface apiService = WSUtil.getRetrofit().create(GithubEndpointInterface.class);
        Call<Repositorio> call = apiService.getRepositories(0);

        call.enqueue(new Callback<Repositorio>() {
            @Override
            public void onResponse(Call<Repositorio> call, Response<Repositorio> response) {
                Log.e("Github", response.message());
                Log.e("Github", response.raw().body().toString());
                Log.e("Github", response.raw().message().toString());

                ArrayList<Repositorio> repos = null ; //= response.body();

            }

            @Override
            public void onFailure(Call<Repositorio> call, Throwable t) {
                Log.e("Github", t.getLocalizedMessage());
            }
        });
    }
*/


    public static int getRepositories(Activity act, int page, WebServiceCallback c) {

        try {
            String mBaseUrl = WebServicesUtils.getBaseUrl();
            String mUrl = mBaseUrl + "/search/repositories?q=language:Java&sort=stars&page=" + page;
            Log.e("Github", "RepositoriBO getRepositories fullurl = "+mUrl);
            WebServicesUtils.get(act, mUrl, c);
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static int getPullRequests(Activity act, WebServiceCallback c, String login, String name, int page) {

        try {
            String mBaseUrl = WebServicesUtils.getBaseUrl();
            String mUrl = mBaseUrl + "/repos/"+login+"/"+name+"/pulls";
            Log.e("Github", "RepositoriBO getPullRequests fullurl = "+mUrl);
            WebServicesUtils.get(act, mUrl, c);
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }

    }
}
